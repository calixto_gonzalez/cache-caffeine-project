package ar.com.cache.caffeine.proyect.controller;

import ar.com.cache.caffeine.proyect.service.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Countries Controller, the whole purpose of this project is to test different cache types.
 */
@RestController
@RequestMapping("/countries")
public class CountriesController {
    private CountriesService countriesService;

    /**
     * Constructor
     *
     * @param countriesService {@link CountriesService}
     */
    @Autowired
    public CountriesController(final CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    /**
     *
     * @param id {@link String} Name of the country of your interest.
     * @return A country with all its info.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getCountry(@PathVariable final String id) {
        try {
            return ResponseEntity.ok(countriesService.getCountry(id));
        } catch (Exception e) {

            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
