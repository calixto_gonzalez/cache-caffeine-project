package ar.com.cache.caffeine.proyect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * App
 */
@SpringBootApplication
@EnableCaching
public class GuavaProyectApplication {
    /**
     *
     * @param args {@link String[]}
     */
    public static void main(final String[] args) {
        SpringApplication.run(GuavaProyectApplication.class, args);
    }
}
