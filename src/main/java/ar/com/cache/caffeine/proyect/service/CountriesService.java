package ar.com.cache.caffeine.proyect.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * Country Service.
 */
@Service
public class CountriesService {
    /**
     * Api
     */
    private RestTemplate restTemplate = new RestTemplate();
    @Value("${url}")
    private String url;

    /**
     * Based on https://docs.spring.io/spring/docs/current/spring-framework-reference/integration.html#cache
     * @param id {@link String}
     * @return Complete information about a country.
     * @throws Exception Throws {@link Exception} to controller.
     */
    @Cacheable(value = "countriesCache", key = "#id")
    public String getCountry(final String id) throws Exception {
        //Builds full URL for the RestCountry API.
        StringBuilder fullUrl = new StringBuilder();

        fullUrl.append(url)
                .append(id);

        String country = restTemplate.getForObject(new URI(fullUrl.toString()), String.class);

        return country;
    }

}
